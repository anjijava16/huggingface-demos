import time

from diffusers import StableDiffusionPipeline
from optimum.intel.openvino import OVStableDiffusionPipeline

prompt = "sailing ship in storm by Rembrandt"

model_id = "runwayml/stable-diffusion-v1-5"

ov_pipe_bf16 = OVStableDiffusionPipeline.from_pretrained(model_id, export=True)

# warmup
images = ov_pipe_bf16(prompt, num_inference_steps=10).images


def elapsed_time(pipeline, nb_pass=2, num_inference_steps=20):
    prompt = "a photo of an astronaut riding a horse on mars"
    start = time.time()
    for _ in range(nb_pass):
        _ = pipeline(prompt, num_inference_steps=num_inference_steps, output_type="np")
    end = time.time()
    return (end - start) / nb_pass


time_ov_model_bf16 = elapsed_time(ov_pipe_bf16)
ov_pipe_bf16.reshape(batch_size=1, height=512, width=512, num_images_per_prompt=1)
images = ov_pipe_bf16(prompt, num_inference_steps=10).images
time_ov_model_bf16_static = elapsed_time(ov_pipe_bf16)

print(time_ov_model_bf16, time_ov_model_bf16_static)
